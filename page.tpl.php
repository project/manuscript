﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

<div class="esas">
	<div id="ust">
		<div class="baslik">

      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" align="left" hspace="10" alt="<?php print t('Home') ?>" /></a><?php } ?>
      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
		
		</div>
	</div>
	
	<div id="ana">

		<div class="merkez">
<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>		
		
		<?php print $breadcrumb ?>
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>		

		
		
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
		</div>
		
		
		<div class="leftmenu">
		<div class="nav">		
		   <?php print $sidebar_left ?>
			</div>
		</div>
	</div>
	
	
	<div id="prefooter">

		<div class="message">

<?php print $footer_message ?>				
			
		</div>
	
	</div>

	<div id="alt">
		<div class="padding">
		Based on: <a href="http://www.free-css-templates.com/free_css_xhtml_templates/i-found-an-old-template/" target="blank">ManuScript</a> | Optimized for Drupal :<a href="http://www.sablonturk.com" title="free web templates">www.SablonTurk.com</a> 
		</div>
	</div>
</div>



<?php print $closure ?>
</body>
</html>
